﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;
using MusicLib.Discogs;
using Newtonsoft.Json;

namespace MusicLib
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        Artist artist;
        public Page2(string ArtistId, string ArtistName)
        {
            InitializeComponent();
            SearchForTheArtist(ArtistId);
            TheTitle.Text = ArtistName;
            loading.IsRunning = false;
            loading.IsVisible = false;
        }
        public async void SearchForTheArtist(string artistId)
        {
            
            string TheContent = await App.FetchJSON("/artists/" + artistId);
            //https://stackoverflow.com/questions/31813055/how-to-handle-null-empty-values-in-jsonconvert-deserializeobject
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
             artist = JsonConvert.DeserializeObject<Artist>(TheContent,settings);
            MatchEvaluator findartist = new MatchEvaluator(ArtID);

            //artist lookup
            //[a#######]->ArtistName , or Num
            artist.Profile = Regex.Replace(artist.Profile, @"\[a\d*]", ArtID);
            //a=ArtistName => [ArtistName]
            artist.Profile = Regex.Replace(artist.Profile, @"[a-z]=","");
            ///#####+, [ , ], [*] => null
            artist.Profile = Regex.Replace(artist.Profile, @"(\[)|(\])|(\[.])|(\[..])|(\d\d\d\d\d{1,})", "");

            TheInfo.Text = artist.Profile;
            Members.ItemsSource = artist.Members;
            ImagesList.ItemsSource = artist.Images;
        }


        public String ArtID(Match match)
        {
            Match onlyNumb = Regex.Match(match.Value, @"\d+");
            String reg = onlyNumb.Value;
            //Console.WriteLine("Found"+ reg);
            //Console.WriteLine(onlyNumb.Value.ToString());
            int convert = int.Parse(onlyNumb.Value);
            //Console.WriteLine(convert.ToString());
            try
            {
                Member artistName = artist.Members.Find(x => x.Id == convert);
                if (artistName != null)
                {
                    //   Console.WriteLine("artist" + artistName.Name);
                    String an = artistName.Name;
                    return an;
                }
                else
                {
                    Console.WriteLine("not found");
                }

                return reg;//"hello";
            }
            catch(Exception exp)
            {
                //DisplayAlert("issue", "errror", "ok");
                return reg;
            }
        }
            
        

    }
}