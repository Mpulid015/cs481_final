﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace MusicLib
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new IntroPage());
        }



        //https://docs.microsoft.com/en-us/xamarin/cross-platform/platform/async
        //adapt parts of wk9 slides
        public static async Task<String> FetchJSON(String fURL)
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                //await DisplayAlert("Disconnected", "Please Connect to the Internat", "OK");
                throw new System.ArgumentException("No Internet");
            }
            else
            {
                String stringJSON;//= null;
                HttpClient fetcher = new HttpClient();
                fetcher.DefaultRequestHeaders.Add("User_Agent", "CS481-Music-App");
                fetcher.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Discogs", "key=HvAgpWBTljHCUHTalCuD, secret=KQXJUAHfHnOgdTeibNePsZZQGUitNxBr");
                String baseurl = "https://api.discogs.com";

                Console.WriteLine("fetching:" + baseurl + fURL + "&key=HvAgpWBTljHCUHTalCuD&secret=KQXJUAHfHnOgdTeibNePsZZQGUitNxBr");

                // Task<string> fetchTask = fetcher.GetStringAsync(baseurl + fURL);

                //suppress simplication message
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get , new Uri(baseurl + fURL));
                //request.RequestUri = new Uri(baseurl + fURL);
                //request.Method = HttpMethod.Get;

                HttpResponseMessage httpWrapper = await fetcher.SendAsync(request);
                if (httpWrapper.IsSuccessStatusCode)
                {
                    Console.WriteLine(httpWrapper.StatusCode.ToString());
                    stringJSON = await httpWrapper.Content.ReadAsStringAsync();
                    //Console.WriteLine(stringJSON);
                    return stringJSON;
                }
                else
                {
                    Console.WriteLine(httpWrapper.StatusCode.ToString());
                    return "";
                }

            }
        }


        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
