﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using MusicLib.Discogs;
using Newtonsoft.Json;


namespace MusicLib
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]




    public partial class MainPage : ContentPage
    {
        public bool help = false;
        public bool explainedOnce = false;
        public MainPage()
        {

            InitializeComponent();

            
        }


        private async void Search(object sender, EventArgs e)
        {
            activityload.IsRunning = true;
            activityload.IsVisible = true;
            String json;


            String url = "/database/search?q=" + thesearch.Text;

            //search options
            if (SearchType.SelectedItem != null) {
                String searchType = SearchType.SelectedItem.ToString().ToLower();

                url += "&type=" + searchType;

            }

            if (GenreType.SelectedItem != null)
            {
                url += "&genre=" + GenreType.SelectedItem.ToString().ToLower();
            }




            Console.WriteLine("Fetching" + url);
            try
            {

                json = await App.FetchJSON(url);

                //        https://stackoverflow.com/questions/31813055/how-to-handle-null-empty-values-in-jsonconvert-deserializeobject
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                SearchResults sResults = JsonConvert.DeserializeObject<SearchResults>(json, settings);

                if (sResults.Results.Count == 0)
                {
                    //MusicList.ItemsSource = null;
                    throw new System.ArgumentException("No results found");
                }

                //TODO: listview of sResults.results  contains a list of search results
                MusicList.ItemsSource = sResults.Results;
                settingActivity();
                //TODO:remove this test
                Console.WriteLine("FROM OBJECT");
                //Console.WriteLine(sResults.Results.ElementAt(0).Title);

            }
            catch (Exception ex)
            {
                await DisplayAlert("Issue", ex.Message, "ok");
                settingActivity();
            }



        }
        private void settingActivity()
        {
            activityload.IsRunning = false;
            activityload.IsVisible = false;
        }

        private async void MusicList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            //TODS Handle what happens when a cell is clicked on
            //Gets the two variables needed for the next page
            var selectedMusic = (Result)e.Item;
            var id = selectedMusic.Id;
            var searchType = selectedMusic.Type;
            if (searchType == "release")
                await Navigation.PushAsync(new Page1(id, searchType));
            if (searchType == "artist")
                await Navigation.PushAsync(new Page2(id.ToString(), selectedMusic.Title));
            if (searchType == "master")
                await Navigation.PushAsync(new MasterPage(id.ToString()));
            if (searchType == "label")
                await Navigation.PushAsync(new LabelPage(id.ToString()));
        }
        //This will clear the entry box
        private async void ClearClicked(object sender, EventArgs e)
        {
            bool Isclear = await DisplayAlert("Clearing", "Are You Sure You Want To Clear", "Yes", "No");
            if (Isclear)
            {
                thesearch.Text = null;
                SearchType.SelectedItem = null;


                GenreType.IsEnabled = true;
                GenreType.IsVisible = true;
                GenreType.SelectedItem = null;
                //clear list?
                // MusicList.ItemsSource = null;

            }
        }

        private void Hide_Genre(object sender, EventArgs e)
        {
            

            if (SearchType.SelectedItem == null)
            {

                GenreType.IsEnabled = true;
                GenreType.IsVisible = true;
                GenreType.SelectedItem = null;

            }
            else if (SearchType.SelectedItem.ToString() == "Artist")
            {
                if (GenreType.IsEnabled)
                {
                    GenreType.SelectedItem = null;
                    GenreType.IsEnabled = false;
                    GenreType.IsVisible = false;
                }
            }
            else
            {
                GenreType.IsEnabled = true;
                GenreType.IsVisible = true;
            }
        }

        private async void SetHelp_Clicked(object sender, EventArgs e)
        {
            //toggle help

            help = !help;
            if (help == true)
            {
                //add context for user 
                Help.Text = "Select an Item for help";
            }
            else
            {
                Help.Text = "Help";
            }
        }

        private void SearchType_Focused(object sender, FocusEventArgs e)
        {
            if (help == true)
            {
                SearchType.IsEnabled = false;
                DisplayAlert("Search Type Filter", "Filter Results to Contain only the Selected Search Type \nOptional", "ok");
                SetHelp_Clicked(sender, e);
                SearchType.IsEnabled = true;
            }
        }

        private void GenreType_Focused(object sender, FocusEventArgs e)
        {
            if (help == true)
            {
                GenreType.IsEnabled = false;
                DisplayAlert("Genre Search Filter Help", "Filter Search results to Contain Only the Selected Genre Type \nNot available for Artist Searches\nOptional", "ok");
                SetHelp_Clicked(sender, e);
                GenreType.IsEnabled = true;
            }
        }

        private void thesearch_Focused(object sender, FocusEventArgs e)
        {
            if (help == true)
            {
                
                DisplayAlert("Search For", "Type an Artist, Song/Release, Album/Master to search for", "ok");
                SetHelp_Clicked(sender, e);
                
            }
        }

   
    }
}
