﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;
using MusicLib.Discogs;
using Newtonsoft.Json;

namespace MusicLib
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1(long musicId, string musicType)
        {
            InitializeComponent();
            
            SetThePage(musicId.ToString());
            loading.IsRunning = false;
            loading.IsVisible = false;

        }

        public async void SetThePage(string MusicId)
        {
            string TheContent = await App.FetchJSON("/releases/" + MusicId);

            //https://stackoverflow.com/questions/31813055/how-to-handle-null-empty-values-in-jsonconvert-deserializeobject
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            Release release = JsonConvert.DeserializeObject<Release>(TheContent,settings);
            TheImage.Source = release.Images.First().Resource_url;
            TheId.Text = release.Title;
            TheType.Text = release.Released;
            Notes.Text = release.Notes;
            
        }

    }
}