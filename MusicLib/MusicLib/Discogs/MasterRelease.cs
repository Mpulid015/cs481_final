﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicLib.Discogs
{
    public class MasterRelease
    {
        public List<String> Styles { get; set; }
        public List<String> Genre { get; set; }
        public List<Video> Videos { get; set; }
        public String title { get; set; }
        public long main_release { get; set; }
        public String main_release_url { get; set; }
        public String Uri { get; set; }
        public List<RArtist> Artists { get; set; }
        public String version_url { get; set; }
        public long Year { get; set; }
        public List<RImage> Images { get; set; }
        public String resource_url { get; set; }
        public List<Tracklists> Tracklist { get; set; }

        public long Id { get; set; }
        public long num_for_sale { get; set; }
        public float lowest_price { get; set; }
        public String data_quality { get; set; }
    }
    public class Video
    {

        public long Duration { get; set; }
        public String Description { get; set; }
        public bool Embed { get; set; }
        public String Uri { get; set; }
        public String Title { get; set; }
    }

    public class Tracklists
    {
        public String duration { get; set; }
        public String position { get; set; }
        public String type_ { get; set; }
        public String title { get; set; }
        public List<ExtraArtist> Extraartists { get; set; }

    }

    public class ExtraArtist{
        public string Join { get; set; }
        public String Name { get; set; }
        public String Anv { get; set; }
        public String Tracks { get; set; }
        public String Role { get; set; }
        public String Resource_url { get; set; }
        public long Id { get; set; }

    }



}
