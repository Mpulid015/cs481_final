﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicLib.Discogs
{

   
    public class Release
    {
        
        public String Title { get; set; }

        public long Id { get; set; }

        public List<RArtist> Artists { get; set; }

        public String Data_quality { get; set; }
        public String Thumb { get; set; }

        public RCommunity Community { get; set; }

        public List<RCompany> Companies { get; set; }
        public String Country { get; set; }
        public String Date_added { get; set; }
        public String Date_changed { get; set; }

        public String Estimated_weight { get; set; }

        public List<RArtist> Extraartists{ get; set; }

        public long Format_quantity { get; set; }


        public List<RImage> Images { get; set; }

        //List<Label> labels { get; set; }

        public float Lowest_price { get; set; }
        public long Master_id { get; set; }
        public String Notes { get; set; }
        public long Num_for_sale { get; set; }
        public String Released { get; set; }
        public String Released_formatted { get; set; }
        public String Resource_url { get; set; }


        //not sure list of type??
        //list<String> series { get; set; }

        public String Status { get; set; }
        public List<String> Styles { get; set; }

        //List<Tracklist> tracklist { get; set; }

        public String Uri { get; set; }
        //List<Videos> videos { get; set; }

        public long Year { get; set; }



        public List<RFormat> Formats { get; set; }
        public List<String> Genres { get; set; }
        public List<RIdentifier> Identifiers { get; set; }




    }
 
//subclasses for Release object
public  class RArtist
    {
        public String Anv { get; set; }
        public long Id { get; set; }
        public String Join { get; set; }
        public String Name { get; set; }
        public String Role { get; set; }
        public String Tracks { get; set; }
    
    //used by master release
    public String resource_url { get; set; }
    }
    public class RCommunity {
        public List<RContributors> Contributors { get; set; }
        public String Data_quality { get; set; }
        public long Have { get; set; }
        public RRating Rating { get; set; }
        public String Status { get; set; }
        public RSubmitter Submitter { get; set; }
        public long Want { get; set; }



    }
    public class RContributors {
        public String Resource_url;
        public String Username;
    
    }
    public class RRating {
        public float Average { get; set; }
        public long Count { get; set; }

    }
    public class RSubmitter {
        public String Resource_url { get; set; }
        public String Username { get; set; }

    }

    public class RCompany
    {
        public String Catno { get; set; }
        public String Entity_type { get; set; }
        public String Entity_type_name { get; set; }
        public long Id { get; set; }
        public String Name { get; set; }
        public String Resource_url { get; set; }
     
    }

    public class RFormat {
        public List<String> Descriptions { get; set; }
        public String Name { get; set; }
        public String Qty { get; set; }
    }

    public class RIdentifier
    {
        public String Type { get; set; }
        public String Value { get; set; }
    }


    public class RImage
    {
        public long Height { get; set; }
        public String Resource_url { get; set; }
        public String Type { get; set; }
        public String Uri { get; set; }
        public String Uri150 { get; set; }
        public long Width { get; set; }

    }


}
