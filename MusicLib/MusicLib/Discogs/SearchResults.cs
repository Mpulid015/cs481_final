﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicLib.Discogs
{
    public class SearchResults
    {
        public Paginations Pageination { get; set; }

        public List<Result> Results { get; set; }


    }



    public class Result
        {
            public List<String> Style { get; set; }
        public String Thumb { get; set; }
        public String Title { get; set; }
        public string Country { get; set; }
        public List<String> Format { get; set; }
        public String Uri { get; set; }
        public SCommunity Community { get; set; }
        public List<String> Label { get; set; }
        public String Catno { get; set; }
        public String Year { get; set; }
        public List<String> Genre{ get; set; }

        public String Resource_uri { get; set; }
        public String Type { get; set; }
        public long Id { get; set; }


        }   
      public  class SCommunity
        {
               public 
            long Want { get; set; }
        public long Have { get; set; }
    




    }
}
