﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicLib.Discogs
{
   public  class Paginations //singular but need 

    {
        public
        long Page { get; set; }
        public long Pages { get; set; }
        public long Items { get; set; }
        public long Per_page { get; set; }
        public URLs Urls { get; set; }

    }

    public class URLs
    {
        public
        String First { get; set; }
        public String Prev { get; set; }
        public String Next { get; set; }
        public String Last { get; set; }

    }
}
