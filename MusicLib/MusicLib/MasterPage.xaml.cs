﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;
using MusicLib.Discogs;
using Newtonsoft.Json;

namespace MusicLib
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public MasterPage(string MasterId)
        {
            InitializeComponent();
            SetPage(MasterId);
            loading.IsRunning = false;
            loading.IsVisible = false;
        }
        public async void SetPage(string MasterId)
        {
            string TheContent = await App.FetchJSON("/masters/" + MasterId);

            //https://stackoverflow.com/questions/31813055/how-to-handle-null-empty-values-in-jsonconvert-deserializeobject
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            MasterRelease release = JsonConvert.DeserializeObject<MasterRelease>(TheContent, settings);
            MTitle.Text = release.title;
            Myear.Text = release.Year.ToString();
            TrackList.ItemsSource = release.Tracklist;
            
        }
    }
}