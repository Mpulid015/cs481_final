﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;
using MusicLib.Discogs;
using Newtonsoft.Json;

namespace MusicLib
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LabelPage : ContentPage
    {
        public LabelPage(string labelid)
        {
            InitializeComponent();
            SetThePage(labelid);
            loading.IsRunning = false;
            loading.IsVisible = false;
        }
        public async void SetThePage(string MusicId)
        {
            string TheContent = await App.FetchJSON("/labels/" + MusicId);

            //https://stackoverflow.com/questions/31813055/how-to-handle-null-empty-values-in-jsonconvert-deserializeobject
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            Labelclass mylabel = JsonConvert.DeserializeObject<Labelclass>(TheContent, settings);
            labelname.Text = mylabel.name;
            labelprofile.Text = mylabel.profile;
            labelcontact.Text = mylabel.contact_info;
            Sublabels.ItemsSource = mylabel.sublabels;
             

            

        }
    }
}